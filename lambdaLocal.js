'use strict';

var usuarios = require('./usuarios.json');
var fs = require('fs');

function editaArquivo(dados){
	fs.writeFile(__dirname+'/usuarios.json', JSON.stringify(dados), 'utf8', function (err) {
		if (err) console.log(err);
	});
}

var res = {
        "statusCode": 200,
        "headers": {
            "x-custom-header" : "my custom header value"
        },
        "body": "{\"status\":\"success\"}"
}


const validaDados = function(dados){
	
	if(!dados.username || dados.username == ''){
		return {status:false,campo:"username"};
	}
	if(!dados.fullname || dados.fullname == ''){
		return {status:false,campo:"fullname"};
	}
	if(!dados.email || dados.email == ''){
		return {status:false,campo:"Email"};
	}else{
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	if(!re.test(dados.email)){
    		return {status:false,campo:"email incorreto"};
    	}
	}
	if(dados.birthdate){
		var data = dados.birthdate.split('/');
		if(data.length != 3){
			return {status:false,campo:"birthdate"};
		}
	}
    //aqui eu limpo os dados para só terem os dados da API, sem pegar dados desnecessários
    var dadosLimpos = {
    	"username":dados.username,
    	"fullname":dados.fullname,
    	"email":dados.email
    }

    if(dados.hobbies){
    	dadosLimpos['hobbies'] = dados.hobbies;
    }
    if(dados.birthdate){
    	dadosLimpos['birthdate'] = dados.birthdate;
    }
    return dadosLimpos;
}

const read = function(id,cb){
	if(id == null){
		let findUsers = {
			TableName:"Usuarios",
			Limit:100
		};	
			
		cb(null,usuarios);
	}else{
		if(isNaN(id)){
			cb("Id Não é um número",null);
		}
		var retorno = {};

		retorno = usuarios.find(function(value){
			return parseInt(value.id) == id;
		})

		cb(null,retorno);
	}
}

const insert = function(dados,cb){
	if(dados == null){
		cb("Não foi passado nenhum dado no corpo da requisição",null);
		return;
	}
	var dados = validaDados(dados);
	if(dados.status === false){
		cb("Erro: "+dados.campo,null);	
	}else{
		var id = new Date().getTime();
		dados['id'] = id;
		try{
			usuarios.push(dados);
			editaArquivo(usuarios);
			cb(null,"Usuário Inserido Com Sucesso");
		}catch(e){
			cb(e,null);
		}
	}
}

const update = function(id,dados,cb){
	// Refatorar
	if(id == null){
		cb("Não foi passado o ID na requisição",null);
		return;
	}
	if(isNaN(id)){
		cb("Id Não é um número",null);
		return;
	}
	if(dados == null){
		cb("Não foi passado nenhum dado no corpo da requisição",null);
		return;
	}
	var dados = validaDados(dados);
	if(dados.status === false){
		cb("Erro: "+dados.campo,null);	
		return ;
	}
	// #Refatorar

	usuarios.forEach(function(usuario){
		if(parseInt(usuario.id) == id){
			usuario.username = dados.username;
			usuario.fullname = dados.fullname;
			usuario.email = dados.email;
			usuario.birthdate = dados.birthdate;
			usuario.hobbies = dados.hobbies;
		}
	});
	try{
		editaArquivo(usuarios);
		cb(null,"Usuário Atualizado Com Sucesso");
	}catch(e){
		cb(e,null);
	}
}

exports.handler = (event, context, callback) => {
	var id = null;
	var dados = null;
	const method = event.requestContext.httpMethod;

	const handleCallback = function(err,dados){
		if(err != null){
			res.body = JSON.stringify(err);
			callback(null,res);
		}
		else{
			res.body = JSON.stringify(dados);
			callback(null,res);
		}
	}

	
	if(event.pathParameters && event.pathParameters != null){
		id = parseInt(event.pathParameters.proxy);
	}

	if(event.body){
		dados = JSON.parse(event.body);	
	}
	

	switch(method){
		case "GET":
			read(id,handleCallback);
			break;
		case "POST":
			insert(dados,handleCallback);
		break;
		case "PUT":
			update(id,dados,handleCallback);
		break;
		default:
		 	var error = "Método não suportado";
		 	res.body = JSON.stringify(error);
    		handleCallback(null,res);
			break;
	}

}