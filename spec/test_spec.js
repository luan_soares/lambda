var req = require('../http-req');
var lambdaLocal = require('../lambdaLocal');

describe("Teste de Requisições Locais", function() {
	describe("GET /", function() {
		it("Return ALL Values From Database", function(done) {
			req.requestContext.httpMethod = "GET";
			req.pathParameters = null;
			lambdaLocal.handler(req,null,function(err,data){
				expect(data.statusCode).toBe(200);
				done();
			});
		});
		it("returns status code 200", function(done) {
			req.requestContext.httpMethod = "GET";
			req.pathParameters = {proxy:1491007176933};
			lambdaLocal.handler(req,null,function(err,data){
				expect(data.statusCode).toBe(200);
				done();
			});
		});
	});
	describe("POST /", function() {
		it("Insert a Value on Database", function(done) {
			req.requestContext.httpMethod = "POST";
			req.pathParameters = null;
			req.pathParameters = null;
			req.body = JSON.stringify({"username":"Luan","fullname":"asdasd","email":"asd@d.com"});
			lambdaLocal.handler(req,null,function(err,data){
				expect(data.statusCode).toBe(200);
				expect(JSON.parse(data.body)).toBe('Usuário Inserido Com Sucesso');
				done();
			});
		});
		it("Throw an error by not passing a Body", function(done) {
			req.requestContext.httpMethod = "POST";
			req.body = null;
			lambdaLocal.handler(req,null,function(err,data){
				expect(data.statusCode).toBe(200);
				expect(JSON.parse(data.body)).toBe('Não foi passado nenhum dado no corpo da requisição');
				done();
			});
		});
	});
	describe("PUT /", function() {
		it("UPDATE a Value on Database", function(done) {
			req.requestContext.httpMethod = "PUT";
			req.pathParameters = {proxy:1491007176933};
			req.body = JSON.stringify({"username":"Luan","fullname":"asdasd","email":"asd@d.com"});
			lambdaLocal.handler(req,null,function(err,data){
				expect(data.statusCode).toBe(200);
				expect(JSON.parse(data.body)).toBe('Usuário Atualizado Com Sucesso');
				done();
			});
		});
		it("Throw an error by not passing ID", function(done) {
			req.requestContext.httpMethod = "PUT";
			req.pathParameters = null;
			req.body = JSON.stringify({"username":"Luan","fullname":"asdasd","email":"asd@d.com"});
			lambdaLocal.handler(req,null,function(err,data){
				expect(data.statusCode).toBe(200);
				expect(JSON.parse(data.body)).toBe('Não foi passado o ID na requisição');
				done();
			});
		});
		it("Throw an error by not passing a Number on ID", function(done) {
			req.requestContext.httpMethod = "PUT";
			req.pathParameters = {proxy:"asd"};
			req.body = JSON.stringify({"username":"Luan","fullname":"asdasd","email":"asd@d.com"});
			lambdaLocal.handler(req,null,function(err,data){
				expect(data.statusCode).toBe(200);
				expect(JSON.parse(data.body)).toBe('Id Não é um número');
				done();
			});
		});
		it("Throw an error by not passing a Body", function(done) {
			req.requestContext.httpMethod = "PUT";
			req.pathParameters = {proxy:1491007176933};
			req.body = null;
			lambdaLocal.handler(req,null,function(err,data){
				expect(data.statusCode).toBe(200);
				expect(JSON.parse(data.body)).toBe('Não foi passado nenhum dado no corpo da requisição');
				done();
			});
		});
	});
});

