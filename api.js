'use strict';

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient({region:'us-west-2'});

var res = {
        "statusCode": 200,
        "headers": {
            "x-custom-header" : "my custom header value"
        },
        "body": "{\"status\":\"success\"}"
}


const validaDados = function(dados){
	
	if(!dados.username || dados.username == ''){
		return {status:false,campo:"username"};
	}
	if(!dados.fullname || dados.fullname == ''){
		return {status:false,campo:"fullname"};
	}
	if(!dados.email || dados.email == ''){
		return {status:false,campo:"Email"};
	}else{
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	if(!re.test(dados.email)){
    		return {status:false,campo:"email incorreto"};
    	}
	}
	if(dados.birthdate){
		var data = dados.birthdate.split('/');
		if(data.length != 3){
			return {status:false,campo:"birthdate"};
		}
	}
    //aqui eu limpo os dados para só terem os dados da API, sem pegar dados desnecessários
    var dadosLimpos = {
    	"username":dados.username,
    	"fullname":dados.fullname,
    	"email":dados.email
    }

    if(dados.hobbies){
    	dadosLimpos['hobbies'] = dados.hobbies;
    }
    if(dados.birthdate){
    	dadosLimpos['birthdate'] = dados.birthdate;
    }
    return dadosLimpos;
}

const read = function(id,cb){
	if(id == null){
		let findUsers = {
			TableName:"Usuarios",
			Limit:100
		};	
		docClient.scan(findUsers,function(err,data){
			if(err){
				cb(err,null)
			}else{
				cb(null,data);
			}
		})
	}else{
		if(isNaN(id)){
			cb("Id Não é um número",null);
			return ;
		}
		let findOne = {
			TableName:"Usuarios",
			Key:{
				id:id
			}
		}
		docClient.get(findOne,function(err,data){
			if(err){
				cb(err,null)
			}else{
				cb(null,data);
			}
		})
	}
}

const insert = function(dados,cb){
	if(dados == null){
		cb("Não foi passado nenhum dado no corpo da requisição",null);
		return ;
	}
	var dados = validaDados(dados);
	if(dados.status === false){
		cb("Erro: "+dados.campo,null);	
	}else{
		var id = new Date().getTime();
		dados['id'] = id;
		var usuario = {
			Item:dados,
			TableName:"Usuarios"
		};
		docClient.put(usuario,function(err,data){
			if(err){
				cb(err,null)
			}else{
				cb(null,"Usuário Inserido Com Sucesso")
			}
		})
	}
}

const update = function(id,dados,cb){
	if(id == null){
		cb("Não foi passado o ID na requisição",null);
		return ;
	}
	if(isNaN(id)){
		cb("Id Não é um número",null);
		return ;
	}
	if(dados == null){
		cb("Não foi passado nenhum dado no corpo da requisição",null);
		return ;
	}
	var dados = validaDados(dados);
	if(dados.status === false){
		cb("Erro: "+dados.campo,null);	
	}else{
		read(id,function(err,retorno){
			var dadosAtuais = retorno.Item;
			//vou passar para os dados atuais, os dados que estou alterando
			
			

			(dados.username ? dadosAtuais['username'] = dados.username : ""); 
			(dados.fullname ? dadosAtuais['fullname'] = dados.fullname : "");
			(dados.email ? dadosAtuais['email'] = dados.email : "");

			// os dois dados a seguir foi que não achei como passar dado vazio para o dynamoDB ao atualizar mas eu não quis perder muito tempo com ele
			(dados.birthdate ? dadosAtuais['birthdate'] = dados.birthdate : (dadosAtuais['birthdate'] ? "" : dadosAtuais['birthdate'] = "11/12/1999"));
			(dados.hobbies ? dadosAtuais['hobbies'] = dados.hobbies : (dadosAtuais['hobbies'] ? "" : dadosAtuais['hobbies'] = ['a']));

			var usuario = {
				TableName:"Usuarios",
				Key: {
					id:id,
				},
				UpdateExpression: "set username = :username,fullname = :fullname,email= :email,birthdate= :birthdate,hobbies= :hobbies",
				ExpressionAttributeValues:{
					":username":dadosAtuais.username,
					":fullname":dadosAtuais.fullname,
					":email":dadosAtuais.email,
					":birthdate":dadosAtuais.birthdate,
					":hobbies":dadosAtuais.hobbies,
				},
				ReturnValues:"UPDATED_NEW"
			};

			docClient.update(usuario,function(err,data){
				if(err){
					cb(err,null)
				}else{
					cb(null,"Usuário Atualizado Com Sucesso")
				}
			});
		});
	}
	

}

exports.handler = (event, context, callback) => {
	var id = null;
	var dados = null;
	const method = event.requestContext.httpMethod;

	const handleCallback = function(err,dados){
		if(err != null){
			res.body = JSON.stringify(err);
			callback(null,res);
		}
		else{
			res.body = JSON.stringify(dados);
			callback(null,res);
		}
	}

	
	if(event.pathParameters && event.pathParameters != null){
		id = parseInt(event.pathParameters.proxy);
	}

	if(event.body){
		dados = JSON.parse(event.body);	
	}
	

	switch(method){
		case "GET":
			read(id,handleCallback);
			break;
		case "POST":
			insert(dados,handleCallback);
		break;
		case "PUT":
			update(id,dados,handleCallback);
		break;
		default:
		 	var error = "Método não suportado";
		 	res.body = JSON.stringify(error);
    		handleCallback(null,res);
			break;
	}

}